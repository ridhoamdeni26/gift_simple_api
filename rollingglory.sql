-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2022 at 06:31 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rollingglory`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_date`, `updated_date`) VALUES
(2, 'phone', '2022-03-27 16:28:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gift`
--

CREATE TABLE `gift` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `point` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `hot_item` int(11) DEFAULT NULL,
  `new_item` int(11) DEFAULT NULL,
  `best_seller` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gift`
--

INSERT INTO `gift` (`id`, `name`, `point`, `description`, `quantity`, `image`, `rate`, `category_id`, `hot_item`, `new_item`, `best_seller`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Samsung Galaxy S9 -Midnight Black 4/64 GB', '200000', 'Ukuran Layar 6.2 inci, Dual Egge Super AMOLED 2960 x 1440 (Quad HD+)  529 ppi, 18 5:9 Memori Ram 6 GB ( LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Andoid 8.0 (Oreo)', 2, 'assets/phone/samsung-galaxy-s9', 3, 1, 1, NULL, NULL, 1, '2022-03-28 08:58:51', NULL),
(2, 'Iphone 11 PRO', '5000000', 'IPHONE 11 PRO NEW', 1, 'assets/phone/iphone-new-11-pro', 4, 1, 1, 1, 1, 1, '2022-03-28 09:12:36', '2022-03-28 09:57:39'),
(3, 'Galaxy S9 -Midnight Black 4/64 GB', '300000', 'Ukuran Layar 6.2 inci, Dual Egge Super AMOLED 2960 x 1440 (Quad HD+)  529 ppi, 18 5:9 Memori Ram 6 GB ( LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Andoid 8.0 (Oreo)', 1, 'assets/phone/samsung-galaxy-s9', 4, 1, 1, 1, NULL, 1, '2022-03-28 09:37:37', NULL),
(4, 'Midnight Black 4/64 GB', '250000', 'Ukuran Layar 6.2 inci, Dual Egge Super AMOLED 2960 x 1440 (Quad HD+)  529 ppi, 18 5:9 Memori Ram 6 GB ( LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Andoid 8.0 (Oreo)', 3, 'assets/phone/samsung-galaxy-s9', 5, 1, 1, 1, NULL, 1, '2022-03-28 09:38:19', NULL),
(5, 'Midnight Black', '250000', 'Ukuran Layar 6.2 inci, Dual Egge Super AMOLED 2960 x 1440 (Quad HD+)  529 ppi, 18 5:9 Memori Ram 6 GB ( LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Andoid 8.0 (Oreo)', 3, 'assets/phone/samsung-galaxy-s9', 3, 1, 1, 1, NULL, 1, '2022-03-28 09:39:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `redeem_gift`
--

CREATE TABLE `redeem_gift` (
  `id` int(11) NOT NULL,
  `name_product` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gift_id` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `redeem_gift`
--

INSERT INTO `redeem_gift` (`id`, `name_product`, `user_id`, `gift_id`, `rate`, `created_date`, `updated_date`) VALUES
(5, 'Samsung Galaxy S9 -Midnight Black 4/64 GB', 3, 1, 3.5, '2022-03-28 10:57:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `point` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `browser` varchar(100) DEFAULT NULL,
  `os` varchar(100) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `point`, `created_date`, `updated_date`, `ip`, `browser`, `os`, `version`) VALUES
(3, 'ridhoamdeni', '$2b$10$tBZaLEqwoRvbpSyCuJqIoeq4obj0lSiiL5YU7xkPuDBDSVR3iNDha', 'ridhoamdeni@rollingglory.com', 5600000, '2022-03-28 08:30:54', '2022-03-28 08:31:22', '192.168.1.100', 'PostmanRuntime', 'unknown', '7.29.0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redeem_gift`
--
ALTER TABLE `redeem_gift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gift`
--
ALTER TABLE `gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `redeem_gift`
--
ALTER TABLE `redeem_gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
