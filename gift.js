var utils = require('./utils');
var database = require('./database');
var express = require('express');
var route = express.Router();
var ip = require('ip');
var moment = require('moment');
var config = require('./config');
const axios = require('axios');
const bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
var ip = require('ip');

route.post('/read', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/read');
   
    var params = {};
    var res_query = "";
    var sort = "";
    var where = "";
    
    params["search"] = req.body.search ? req.body.search : null;
    params["status"] = req.body.status ? req.body.status.toString() : null;
    params["page"] = req.body.page ? req.body.page.toString() : "0";    
    params["offset"] = req.body.offset ? req.body.offset.toString() : "3000";
    params["flag_show"] = req.body.flag_show ? req.body.flag_show : null;
    params["sort"] = req.body.sort ? req.body.sort : null;
    params["rate"] = req.body.rate ? req.body.rate : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;

                    if(params.search){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' (gift.fullname LIKE CONCAT("%",:search,"%") OR gift.email LIKE CONCAT("%",:search,"%")) ';
                    }

                    if (params.sort == 1) {
                        if (params.rate == null) {
                            sort += 'ORDER BY gift.created_date DESC'
                        }else {
                            sort += 'ORDER BY CAST(rate AS DOUBLE) DESC'
                        }
                    }else {
                        sort += 'ORDER BY gift.created_date ASC'
                    }
                    
                    var res_show = "";
                    if(params.flag_show!=1){
                        res_show +=' LIMIT :page,:offset ';
                    }

                    var query = "SELECT gift.* FROM gift "+res_query+sort+res_show;


                    console.log(connection.format(query,params));
                    connection.execute(query, params, function(err, rows, cols){
                        if(err){
                            data_response(req,res,500,err,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){
                                for (var i = 0; i < rows.length; i++) {
                                    if(rows[i].dob!=null || rows[i].dob!=undefined ){
                                        rows[i].dob=moment(rows[i].dob).format('DD MMM YYYY');
                                    }
                                    if(rows[i].created_date!=null || rows[i].created_date!=undefined ){
                                        rows[i].created_date=moment(rows[i].created_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    if(rows[i].updated_date!=null || rows[i].updated_date!=undefined ){
                                        rows[i].updated_date=moment(rows[i].updated_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                }
                                if(params.flag_show==1){
                                    data_response(req,res,200,null,"Found", rows, null, "total");
                                }
                                else{
                                    data_response(req,res,200,null,"Found", rows, null, "read");
                                }
                            }
                            else{   
                                data_response(req,res,200,null,"Not found", rows, null, "read");
                            }
                        }
                    });
                    connection.release();
                });
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/create',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/create');

    var params = {};
    
    params['name']=req.body.name ? req.body.name : null;
    params['point']=req.body.point ? req.body.point : null;
    params['description']=req.body.description ? req.body.description : null;
    params['quantity']=req.body.quantity ? req.body.quantity : null;
    params['image']=req.body.image ? req.body.image : null;
    params['rate']=req.body.rate ? req.body.rate : null;
    params['category_id']=req.body.category_id ? req.body.category_id : null;

    params['hot_item']=req.body.hot_item ? req.body.hot_item : null;
    params['new_item']=req.body.new_item ? req.body.new_item : null;
    params['best_seller']=req.body.best_seller ? req.body.best_seller : null;
    params['status']=req.body.status ? req.body.status : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200) {
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,401,null,'Name is required',null,null);
                }
                else{
                    database.getConnection(function(err,connection){
                        connection.config.namedPlaceholders=true;

                        params['rate'] = Math.round(params.rate*2)/2;

                        var query = "INSERT INTO gift (name, point, description, quantity, image, rate, category_id, hot_item, new_item, best_seller, status, created_date) VALUES (:name, :point, :description, :quantity, :image, :rate, :category_id, :hot_item, :new_item, :best_seller, :status, NOW())";
                        
                        console.log(connection.format(query,params));

                        connection.execute(query, params, function(err,rows,cols){
                            if(err){
                                if(err){
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{ 
                                if(rows["affectedRows"]>0){
                                    data_response(req,res,200,null,"Success",null,null,'create');
                                }
                                else{
                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/update', function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/update');

    var params = {};

    params['id']=req.body.id ? req.body.id : null;
    params['name']=req.body.name ? req.body.name : null;
    params['point']=req.body.point ? req.body.point : null;
    params['description']=req.body.description ? req.body.description : null;
    params['quantity']=req.body.quantity ? req.body.quantity : null;
    params['image']=req.body.image ? req.body.image : null;
    params['rate']=req.body.rate ? req.body.rate : null;
    params['category_id']=req.body.category_id ? req.body.category_id : null;

    params['hot_item']=req.body.hot_item ? req.body.hot_item : null;
    params['new_item']=req.body.new_item ? req.body.new_item : null;
    params['best_seller']=req.body.best_seller ? req.body.best_seller : null;
    params['status']=req.body.status ? req.body.status : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,400,null,'Name is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;

                        params['rate'] = Math.round(params.rate*2)/2;
                
                        var query = "UPDATE gift SET name=:name, point=:point, description=:description, quantity=:quantity, image=:image, rate=:rate, category_id=:category_id, hot_item=:hot_item, new_item=:new_item, best_seller=:best_seller, status=:status, updated_date=NOW() WHERE id=:id";

                        console.log(connection.format(query,params))
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                if(err){
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                                    data_response(req,res,200,null,"Success",null,null,'update');         
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"update");
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });
    }
});

route.post('/delete',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/delete');
    var params = {};
    params['id']=req.body.id ? req.body.id : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.id=="" || params.id==undefined || params.id==null){
                    data_response(req,res,401,null,'ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                        
                        var query = "DELETE FROM gift WHERE id=:id";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                data_response(req,res,500,err,null,connection.format(query,params));
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                                    data_response(req,res,200,null,"Success",null,null,"delete");
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"delete");
                                }
                            }
                        });
                        connection.release();
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

route.post('/redeem',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/delete');
    var params = {};

    params['user_id']=req.body.user_id ? req.body.user_id : null;
    params['gift_id']=req.body.gift_id ? req.body.gift_id : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.user_id=="" || params.user_id==undefined || params.user_id==null){
                    data_response(req,res,401,null,'USER ID is required',null,null);
                }else if (params.gift_id=="" || params.gift_id==undefined || params.gift_id==null){
                    data_response(req,res,401,null,'GIFT ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                        
                        var query = "SELECT name, point FROM users WHERE id=:user_id LIMIT 1";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                data_response(req,res,500,err,null,connection.format(query,params));
                            }
                            else{
                                if(rows.length > 0){
                                    var query = "SELECT gift.* FROM gift WHERE id =:gift_id";

                                    connection.execute(query, params, function(err, rows2, cols){
                                        if(err){
                                            data_response(req,res,500,err,null,null,connection.format(query,params));
                                        }
                                        else{
                                            if(rows2.length > 0){
                                                var point_user = rows[0].point;
                                                var point_gift = rows2[0].point;
                                                var stock_gift = rows2[0].quantity;

                                                if (point_user < point_gift) {

                                                    var code = 400;
                                                    var response = {
                                                        status : { code : code, message : "Insufficient user point"},
                                                    }   
                                                    
                                                    return utils.outputData(req, res, code, err, response);
                                                }
                                                else {

                                                    if (stock_gift > 0) {

                                                        params['name_product'] = rows2[0].name;
                                                        
                                                        var query = "INSERT INTO redeem_gift (name_product, user_id, gift_id, created_date) VALUES (:name_product, :user_id, :gift_id, NOW())";
                                    
                                                        connection.execute(query, params, function(err,rows3,cols){
                                                            if(err){
                                                                data_response(req,res,500,err,null,connection.format(query,params));
                                                            }
                                                            else{ 
                                                                if(rows3["affectedRows"]>0){
                                                                    params['res_point'] = point_user - point_gift;

                                                                    var query = "UPDATE users SET point=:res_point WHERE id=:user_id LIMIT 1";
                        
                                                                    connection.execute(query, params, function(err, rows4, cols){
                                                                        
                                                                        if(err){
                                                                            data_response(req,res,500,err,null,connection.format(query,params));
                                                                        }
                                                                        else{
                                                                            if(rows4["affectedRows"] > 0){

                                                                                data_response(req,res,200,null,"Success",null,null,'update');
                                                                                        
                                                                            }
                                                                            else{
                                                                                data_response(req,res,200,null,"Not found",null,null,"update");
                                                                            }
                                                                        }
                                                                    });
                                                                    connection.release();
                                                                }
                                                                else{
                                                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                                }
                                                            }
                                                        });
                                                        connection.release();

                                                    }
                                                    else {
                                                        var code = 400;
                                                        var response = {
                                                            status : { code : code, message : "Product do not have stock"},
                                                        }

                                                        return utils.outputData(req, res, code, err, response);
                                                    }

                                                }
                                            }
                                            else{   
                                                data_response(req,res,200,null,"Not found", rows2, null, "read");
                                            }
                                        }
                                    });
                                    connection.release();
                                }
                                else{   
                                    data_response(req,res,200,null,"Not found", rows, null, "read");
                                }

                            }
                        });
                        connection.release();
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

route.post('/rating',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'gift/delete');
    var params = {};

    params['user_id']=req.body.user_id ? req.body.user_id : null;
    params['redeem_id']=req.body.redeem_id ? req.body.redeem_id : null;
    params['rate']=req.body.rate ? req.body.rate : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.user_id=="" || params.user_id==undefined || params.user_id==null){
                    data_response(req,res,401,null,'USER ID is required',null,null);
                }else if (params.redeem_id=="" || params.redeem_id==undefined || params.redeem_id==null){
                    data_response(req,res,401,null,'Redeem ID is required',null,null);
                }
                else if (params.rate=="" || params.rate==undefined || params.rate==null){
                    data_response(req,res,401,null,'Rate is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                        
                        var query = "SELECT redeem_gift.* FROM redeem_gift WHERE user_id=:user_id LIMIT 1";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                data_response(req,res,500,err,null,connection.format(query,params));
                            }
                            else{
                                if(rows.length > 0){
                                    
                                    var rate = rows[0].rate

                                    if (rate == null) {
                                        
                                        params['rate'] = Math.round(params.rate*2)/2;

                                        var query = "UPDATE redeem_gift SET rate=:rate WHERE user_id=:user_id AND id=:redeem_id LIMIT 1";
                        
                                        connection.execute(query, params, function(err, rows4, cols){
                                            
                                            if(err){
                                                data_response(req,res,500,err,null,connection.format(query,params));
                                            }
                                            else{
                                                if(rows4["affectedRows"] > 0){
                                                    data_response(req,res,200,null,"Thank you for rating this product",null,null,'update');    
                                                }
                                                else{
                                                    data_response(req,res,200,null,"Not found",null,null,"update");
                                                }
                                            }
                                        });
                                        connection.release();

                                    }
                                    else{

                                        var code = 400;
                                        var response = {
                                            status : { code : code, message : "Sorry you have given a rating on this product"},
                                        }

                                        return utils.outputData(req, res, code, err, response);

                                    }

                                }
                                else{   
                                    data_response(req,res,200,null,"Not found", rows, null, "read");
                                }

                            }
                        });
                        connection.release();
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

function data_response(req,res,code,err,dtmessage,rows,dtquery,act){
    var response = {};
    if(code==200){
        if(act=="total"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length }
            }    
        }
        else if(act=="read"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length, rows }
            }    
        }
        else if(act=="detail"){
            response = {
                status : { code : code, message : dtmessage},
                data : rows 
            }    
        }
        else if(act=="delete" || act=="create" || act=="update"){
            response = {
                status : { code : code, message : dtmessage},
            }    
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==400){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==401){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else{
        response = {
            status : { code : 500, message : 'Internal Server Error', query:dtquery, err:err}
        }
        return utils.outputData(req, res, code, err, response);
    }
}

module.exports = route;