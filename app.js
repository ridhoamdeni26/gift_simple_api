'use strict';

var config = require('./config');
var express = require('express');
var bodyparser = require('body-parser');
var app = express();
var useragent = require('express-useragent');

var cors = require('cors');
    app.options('*', cors());
    app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,x-token,application-id,module-id');
    next();
});

app.use(useragent.express());
app.use(function(req, res, next) {
    res.locals.ua = req.useragent;
    var check = {
        browser: res.locals.ua.browser,
        version: res.locals.ua.version,
        os: res.locals.ua.os,
        platform: res.locals.ua.platform
     }
    //  console.log(JSON.stringify(res.locals.ua, null ,4))
     next()
});


app.use(bodyparser.urlencoded({
    extended : true
}));
app.use(bodyparser.json({
    extended : false
}));

var users = require('./users');
var category = require('./category');
var gift = require('./gift');

app.use('/users',users);
app.use('/category',category);
app.use('/gift',gift);

app.get('/', (req,res) => res.send(config.app_name));
app.listen(config.app_port, config.app_host);
// srv.listen(config.app_port);
console.log(config.app_name + ' running on port ' +config.app_port);