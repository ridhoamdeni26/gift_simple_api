'use strict';

var config = {};

config.secret_key = process.env.SECRET_KEY || 'api-rollingglory';
config.token_expired = 3600 * 12;
config.token_expired_mobile = 31557600000;
config.token_code = 'AKIAXRW6HM3I7D2ZWZ6J'

config.app_name = process.env.APP_NAME || 'API-ROLLING-GLORY';
config.app_port = process.env.APP_PORT || '3000';
config.app_host = process.env.APP_HOST || '0.0.0.0';
config.db_host = process.env.DB_HOST || '127.0.0.1';
config.db_user = process.env.DB_USER || 'root';
config.db_pass = process.env.DB_PASSWORD || '';
config.db_name = process.env.DB_NAME || 'rollingglory';
config.db_port = process.env.DB_PORT || '3306';

module.exports = config;