'use strict';

var utils = require('./utils');
var database = require('./database');
var express = require('express');
var route = express.Router();
var ip = require('ip');
var moment = require('moment');
var config = require('./config');
const axios = require('axios');
const bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
var ip = require('ip');
var AWS = require('aws-sdk');
const multer = require('multer');
var multerS3 = require('multer-s3');
var s3 = new AWS.S3({ 
    accessKeyId: config.iam_access_key_id,
    secretAccessKey: config.iam_secret_access_key
})

var upload_customer = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'maimaid',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
        // cb(null, Object.assign({}, req.body));
    },
    key: function (req, file, cb) {
        console.log('REQ : '+req);
        console.log('FILE : '+file);
        console.log('CB : '+cb);
        cb(null, 'customer/'+Date.now().toString())
        // cb(null, req.body.image_profile + ".png");
    },
  })
})

route.post('/login', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/login');
    var dtArray = [];
    var params = {};

    params["phone"] = req.body.phone ? req.body.phone : null;
    params["token_short"] = req.body.token_short ? req.body.token_short : null;
    params['flag_prod']=req.body.flag_prod ? req.body.flag_prod : null;
    params['token_identity']=req.body.token_identity ? req.body.token_identity : null;

    const token = req.headers['x-token'];

    var res_app_id="";
    var res_api_key="";
    var res_otp=null;
    if(params.flag_prod==1){
        res_app_id=config.vh_app_id_prod;
        res_api_key=config.vh_api_key_prod;
    }
    else{
        res_app_id=config.vh_app_id;
        res_api_key=config.vh_api_key;
        res_otp="123123";
    }

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;
                    
                    var query = "SELECT customer.*, customer_status.name as customer_status_name FROM customer JOIN customer_status ON customer_status.id=customer.status WHERE phone=:phone LIMIT 1";
                    
                    connection.execute(query, params, function(err, rows, cols){
                        
                        if(err){
                            data_response(req,res,500,err,null,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){

                                if(rows[0].status=="2"){
                                    data_response(req,res,400,err,'Not Active',null,null);
                                }
                                else{
                                    var res_token="";
                                    
                                    if(params.token_short==1){
                                        res_token=config.token_expired;
                                    }
                                    else{
                                        res_token=config.token_expired_mobile;
                                    }
                                    
                                    var token = jwt.sign(
                                        {
                                            id: rows[0]['id']
                                        }, 
                                        config.secret_key, { 
                                            expiresIn: res_token
                                        }
                                    );

                                    var user_flag = "1";

                                    var query = "SELECT customer_maipay.* FROM customer_maipay WHERE customer_maipay.user_flag='"+user_flag+"' AND customer_maipay.user_id='"+rows[0].id+"' LIMIT 1";
                                    
                                    connection.execute(query, params, function(err, rows3, cols){
                                        
                                        if(err){
                                            data_response(req,res,500,err,null,null,connection.format(query,params));
                                        }
                                        else{
                                            if(rows3.length > 0){


                                                var query = "SELECT id FROM partner WHERE customer_id='"+rows[0].id+"' LIMIT 1";
                                                connection.execute(query, params, function(err, rows7, cols){
                                                    if(err){
                                                        data_response(req,res,500,err,null,null,connection.format(query,params));
                                                    }
                                                    else{
                                                        var res_partner=null;
                                                        if(rows7[0]!=undefined){
                                                            res_partner=1;
                                                        }

                                                        var res_verification_data=null;
                                                        if(rows[0].image_idcard && rows[0].image_selfie && rows[0].image_idcard_selfie){
                                                            res_verification_data=1;
                                                        }
                                                        
                                                        const options = {
                                                            method: 'POST',
                                                            url: config.link_verihubs+'/v1/otp/send',
                                                            headers: {
                                                                Accept: 'application/json',
                                                                'App-ID': res_app_id,
                                                                'API-Key': res_api_key,
                                                                'Content-Type': 'application/json'
                                                            },
                                                            data: {
                                                                msisdn: params.phone,
                                                                otp : res_otp,
                                                                time_limit: '60',
                                                            }
                                                        };
                                            
                                                        axios.request(options).then(function (response) {
                                                            
                                                            params['response_otp'] = response.data.otp;
        
                                                            var query = "UPDATE customer SET otp_key=:response_otp WHERE phone=:phone";
                                
                                                            connection.execute(query, params, function(err, rows4, cols){
                                                                
                                                                if(err){
                                                                    data_response(req,res,500,err,null,connection.format(query,params));
                                                                }
                                                                else{
                                                                    if(rows4["affectedRows"] > 0){
        
                                                                        var query = "SELECT token_fcm FROM user_fcm_customer WHERE token_fcm=:token_identity AND user_fcm_customer.user_id='"+rows[0].id+"' LIMIT 1";
        
                                                                        connection.execute(query, params, function(err, rows5, cols){
                                                                            if(err){
                                                                                data_response(req,res,500,err,null,null,connection.format(query,params));
                                                                            }
                                                                            else{
                                                                                
                                                                                var res_pass="";
                                                                                if(rows[0]['pin']){
                                                                                    res_pass=1;
                                                                                }
                                                                                else{
                                                                                    res_pass=null;
                                                                                }
        
                                                                                if(rows5[0]==undefined){
                                                                                    var query = "INSERT INTO user_fcm_customer (user_id, token_fcm, status, created_date) VALUES ('"+rows[0].id+"', :token_identity, 1,  NOW())";
                                                                                    connection.execute(query, params, function(err,rows6,cols){
                                                                                        if(err){
                                                                                            data_response(req,res,500,err,null,connection.format(query,params));
                                                                                        }
                                                                                        else{ 
                                                                                            
                                                                                            if(rows6["affectedRows"]>0){
                                                                                                
                                                                                                dtArray = { 
                                                                                                    id:rows[0].id,
                                                                                                    customer_name:rows[0].fullname,
                                                                                                    customer_phone:rows[0].phone,
                                                                                                    customer_email:rows[0].email,
                                                                                                    customer_maipay_id:rows3[0].id,
                                                                                                    user_flag:user_flag,
                                                                                                    pin:res_pass,
                                                                                                    partner:res_partner,
                                                                                                    verification_data:res_verification_data,
                                                                                                    token:token,
                                                                                                    expired:res_token,
                                                                                                };
                                                                                                data_response(req,res,200,null,"Found", dtArray, null,"detail");
                                                                                            }
                                                                                            else{
                                                                                                data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                    connection.release();
                                                                                }
                                                                                else{
                                                                                    
                                                                                    dtArray = { 
                                                                                        id:rows[0].id,
                                                                                        customer_name:rows[0].fullname,
                                                                                        customer_phone:rows[0].phone,
                                                                                        customer_email:rows[0].email,
                                                                                        customer_maipay_id:rows3[0].id,
                                                                                        user_flag:user_flag,
                                                                                        pin:res_pass,
                                                                                        partner:res_partner,
                                                                                        verification_data:res_verification_data,
                                                                                        token:token,
                                                                                        expired:res_token,
                                                                                    };
                                                                                    data_response(req,res,200,null,"Found", dtArray, null,"detail");
                                                                                }
                                                                            }
                                                                        });
                                                                        connection.release();
                                                                    }
                                                                    else{
                                                                        data_response(req,res,200,null,"Not found",null,null,"update");
                                                                    }
                                                                }
                                                            });
                                                            connection.release();
        
                                                        }).catch(function (error) {
                                                            console.log(error);
                                                            data_response(req,res,401,err,error.response.data.message,null,null);
                                                        });    
                                                    }
                                                });
                                                connection.release();
   
                                            }
                                            else{   
                                                data_response(req,res,200,null,"Not Found", dtArray, null,"detail");
                                            }
                                        }
                                    });
                                    connection.release();     
                                }  
                            }
                            else{ 
                                data_response(req,res,400,null,"Email / phone or password is wrong", rows, null,"detail");
                            }
                        }
                    });
                    connection.release();
                });
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/change_password',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/change_password');
    var params = {};

    params["id"] = req.body.id ? req.body.id : null;
    params["old_password"] = req.body.old_password ? req.body.old_password : null;
    params["new_password"] = req.body.new_password ? req.body.new_password : null;
    params["updated_by"] = req.body.updated_by ? req.body.updated_by : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200) {	
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders=true;

                    check_password(connection, params.id, params.old_password, function(result) {
                        if (result.err) {
                            data_response(req,res,500,err,null,null,connection.format(query,params));
                        }
                        else if(result!="" && result!=undefined && result!=null && result!=[]){
                            params["res_new_password"] =md5(params.new_password).toString();
                            var query = "UPDATE customer SET password=:res_new_password, updated_by=:updated_by WHERE id=:id";
                            
                            connection.execute(query, params, function(err, rows, cols){
                                if(err){
                                    data_response(req,res,500,err,null,null,connection.format(query,params));
                                }
                                else{
                                    if(rows["affectedRows"] > 0){
                                        data_response(req,res,200,null,"Success",null,null,"update");
                                    }
                                    else{
                                        data_response(req,res,200,null,"Not Found",null,null,"update");
                                    }
                                }
                            });
                            connection.release();
                        }
                        else {
                            data_response(req,res,401,null,'Old Password Wrong',null,null);
                        }
                    });
                })
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        })
    }
});

route.post('/recognition', 
    upload_customer.fields([{
    name: 'image_idcard', maxCount: 1}, {name: 'image_selfie', maxCount: 1}, {name: 'image_idcard_selfie', maxCount: 1} ]),
    function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/recognition');
   
    var params = {};

    params['id']=req.body.id ? req.body.id : null;
    params['image_a']=req.body.image_a ? req.body.image_a : null;
    params['image_b']=req.body.image_b ? req.body.image_b : null;
    params['flag_prod']=req.body.flag_prod ? req.body.flag_prod : null;
 
    if(req.files){
        params['image_idcard']=req.files.image_idcard ? req.files.image_idcard[0].location : null;
        params['image_selfie']=req.files.image_selfie ? req.files.image_selfie[0].location : null;
        params['image_idcard_selfie']=req.files.image_idcard_selfie ? req.files.image_idcard_selfie[0].location : null;
    }
    else{
        params['image_idcard']="";
        params['image_selfie']="";
        params['image_idcard_selfie']="";
    }

    const token = req.headers['x-token'];

    var res_app_id="";
    var res_api_key="";
    var res_image_a="";
    var res_image_b="";
    
    if(params.flag_prod==1){
        res_app_id=config.vh_app_id_prod;
        res_api_key=config.vh_api_key_prod;
        res_image_a=params.image_idcard;
        res_image_b=params.image_selfie;
    }
    else{
        res_app_id=config.vh_app_id;
        res_api_key=config.vh_api_key;
        res_image_a=params.image_a;
        res_image_b=params.image_b;
    }
 
    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) { 
            database.getConnection(function(err, connection){
                connection.config.namedPlaceholders = true;
 
                const options = {
                    method: 'POST',
                    url: config.link_verihubs+'/v1/face/enroll',
                    headers: {
                        Accept: 'application/json',
                        'App-ID': res_app_id,
                        'API-Key': res_api_key,
                        'Content-Type': 'application/json'
                    },
                    data: {
                        image: res_image_a
                        // image: params.image_selfie
                    }
                };
 
                axios.request(options).then(function (response) {

                    params['subject_id']=response.data.subject_id;
                    params['recognition_enroll']=response.data;

                    var query = "UPDATE customer SET subject_id=:subject_id, recognition_enroll=:recognition_enroll, image_idcard=:image_idcard, image_selfie=:image_selfie, image_idcard_selfie=:image_idcard_selfie WHERE id=:id";
                        
                    connection.execute(query, params, function(err, rows, cols){
                        
                        if(err){
                            data_response(req,res,500,err,null,connection.format(query,params));
                        }
                        else{
                            if(rows["affectedRows"] > 0){

                                const options = {
                                    method: 'POST',
                                    url: config.link_verihubs+'/v1/face/compare',
                                    headers: {
                                        Accept: 'application/json',
                                        'App-ID': res_app_id,
                                        'API-Key': res_api_key,
                                        'Content-Type': 'application/json'
                                    },
                                    data: {
                                        image_1: res_image_a,
                                        image_2: res_image_b
                                        // image_1: params.image_selfie,
                                        // image_2: params.image_idcard
                                    }
                                };
                
                                axios.request(options).then(function (response2) {
                                    console.log(response2.data);
                                    
                                    if(response2.data.similarity<60){
                                        var code = 400;
                                        var dtmessage = "Fail";
                                    }
                                    else{
                                        var code = 200;
                                        var dtmessage = "Success";
                                    }
                                    response = {
                                        status : { code : code, message : dtmessage },
                                        data : { 
                                            similarity : response2.data.similarity,
                                            image_condition_a : response2.data.image_1,
                                            image_condition_b : response2.data.image_2
                                        }
                                    }
                                    return utils.outputData(req, res, code, err, response);
                                }).catch(function (error) {
                                    console.error(error);
                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                });
                            }
                            else{
                                data_response(req,res,200,null,"Not found",null,null,"update");
                            }
                        }
                    });
                    connection.release();
                }).catch(function (error) {
                    console.error(error);
                    data_response(req,res,400,err,error.response.data.message,null,null);
                });
                connection.release();
            });
        });        
    }
});

route.post('/otp_verify', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/otp_verify');
   
    var dtArray = [];
    var params = {};
    var res_query = "";
    var where = "";
    
    params["otp"] = req.body.otp ? req.body.otp : null;
    params["phone"] = req.body.phone ? req.body.phone : null;
    params["type"] = req.body.type ? req.body.type : null;
    params['flag_prod']=req.body.flag_prod ? req.body.flag_prod : null;
    params['token_identity']=req.body.token_identity ? req.body.token_identity : null;
    
    const token = req.headers['x-token'];

    var res_app_id="";
    var res_api_key="";

    if(params.flag_prod==1){
        res_app_id=config.vh_app_id_prod;
        res_api_key=config.vh_api_key_prod;
    }
    else{
        res_app_id=config.vh_app_id;
        res_api_key=config.vh_api_key;
    }
 
    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {	
                if(params.otp=="" || params.otp==undefined || params.otp==null){
                    data_response(req,res,400,null,'OTP is required',null,null);
                }
                else if(params.phone=="" || params.phone==undefined || params.phone==null){
                    data_response(req,res,400,null,'Phone is required',null,null);
                }
                else if(params.type=="" || params.type==undefined || params.type==null){
                    data_response(req,res,400,null,'Type is required',null,null);
                }
                else{	
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders = true;

                        const options = {
                            method: 'POST',
                            url: config.link_verihubs+'/v1/otp/verify',
                            headers: {
                                Accept: 'application/json',
                                'App-ID': res_app_id,
                                'API-Key': res_api_key,
                                'Content-Type': 'application/json'
                            },
                            data: {msisdn: params.phone, otp: params.otp}
                        };
        
                        axios.request(options).then(function (response) {
                            if(response.data.code==200){
                                var query = "SELECT customer.*, customer_status.name as customer_status_name FROM customer JOIN customer_status ON customer_status.id=customer.status WHERE phone=:phone AND otp_key=:otp LIMIT 1";
                            
                                connection.execute(query, params, function(err, rows, cols){
                                    
                                    if(err){
                                        data_response(req,res,500,err,null,null,connection.format(query,params));
                                    }
                                    else{
                                        if(rows.length > 0){

                                            if(rows[0].status=="2"){
                                                data_response(req,res,400,err,'Not Active',null,null);
                                            }
                                            else{
                                                var res_token="";
                                                
                                                if(params.token_short==1){
                                                    res_token=config.token_expired;
                                                }
                                                else{
                                                    res_token=config.token_expired_mobile;
                                                }
                                                
                                                var token = jwt.sign(
                                                    {
                                                        id: rows[0]['id']
                                                    }, 
                                                    config.secret_key, { 
                                                        expiresIn: res_token
                                                    }
                                                );

                                                var user_flag = "1";

                                                var query = "SELECT customer_maipay.* FROM customer_maipay WHERE customer_maipay.user_flag='"+user_flag+"' AND customer_maipay.user_id='"+rows[0].id+"' LIMIT 1";
                                                
                                                connection.execute(query, params, function(err, rows3, cols){
                                                    
                                                    if(err){
                                                        data_response(req,res,500,err,null,null,connection.format(query,params));
                                                    }
                                                    else{
                                                        if(rows3.length > 0){

                                                            var query = "SELECT id FROM partner WHERE customer_id='"+rows[0].id+"' LIMIT 1";
                                                            connection.execute(query, params, function(err, rows7, cols){
                                                                if(err){
                                                                    data_response(req,res,500,err,null,null,connection.format(query,params));
                                                                }
                                                                else{
                                                                    var res_partner=null;
                                                                    if(rows7[0]!=undefined){
                                                                        res_partner=1;
                                                                    }

                                                                    var res_verification_data=null;
                                                                    if(rows[0].image_idcard && rows[0].image_selfie && rows[0].image_idcard_selfie){
                                                                        res_verification_data=1;
                                                                    }

                                                                    var res_query = "";
                                                                    if(params.type==2){
                                                                        res_query = 'status=1,';
                                                                    }

                                                                    var query = "UPDATE customer SET "+res_query+" otp_key=NULL WHERE phone=:phone";
                                        
                                                                    connection.execute(query, params, function(err, rows4, cols){
                                                                        
                                                                        if(err){
                                                                            data_response(req,res,500,err,null,connection.format(query,params));
                                                                        }
                                                                        else{
                                                                            if(rows4["affectedRows"] > 0){

                                                                                var query = "SELECT token_fcm FROM user_fcm_customer WHERE token_fcm=:token_identity AND user_fcm_customer.user_id='"+rows[0].id+"' LIMIT 1";

                                                                                connection.execute(query, params, function(err, rows5, cols){
                                                                                    if(err){
                                                                                        data_response(req,res,500,err,null,null,connection.format(query,params));
                                                                                    }
                                                                                    else{
                                                                                        
                                                                                        var user_flag = "1";

                                                                                        var res_pass="";
                                                                                        if(rows[0]['pin']){
                                                                                            res_pass=1;
                                                                                        }
                                                                                        else{
                                                                                            res_pass=null;
                                                                                        }

                                                                                        if(rows5[0]==undefined){
                                                                                            var query = "INSERT INTO user_fcm_customer (user_id, token_fcm, status, created_date) VALUES ('"+rows[0].id+"', :token_identity, 1,  NOW())";
                                                                                            connection.execute(query, params, function(err,rows6,cols){
                                                                                                if(err){
                                                                                                    data_response(req,res,500,err,null,connection.format(query,params));
                                                                                                }
                                                                                                else{ 
                                                                                                    
                                                                                                    if(rows6["affectedRows"]>0){
                                                                                                        
                                                                                                        dtArray = { 
                                                                                                            id:rows[0].id,
                                                                                                            customer_name:rows[0].fullname,
                                                                                                            customer_phone:rows[0].phone,
                                                                                                            customer_email:rows[0].email,
                                                                                                            customer_maipay_id:rows3[0].id,
                                                                                                            user_flag:user_flag,
                                                                                                            pin:res_pass,
                                                                                                            partner:res_partner,
                                                                                                            verification_data:res_verification_data,
                                                                                                            token:token,
                                                                                                            expired:res_token,
                                                                                                        };
                                                                                                        data_response(req,res,200,null,"Found", dtArray, null,"detail");
                                                                                                    }
                                                                                                    else{
                                                                                                        data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            connection.release();
                                                                                        }
                                                                                        else{
                                                                                            
                                                                                            dtArray = { 
                                                                                                id:rows[0].id,
                                                                                                customer_name:rows[0].fullname,
                                                                                                customer_phone:rows[0].phone,
                                                                                                customer_email:rows[0].email,
                                                                                                customer_maipay_id:rows3[0].id,
                                                                                                user_flag:user_flag,
                                                                                                pin:res_pass,
                                                                                                partner:res_partner,
                                                                                                verification_data:res_verification_data,
                                                                                                token:token,
                                                                                                expired:res_token,
                                                                                            };
                                                                                            data_response(req,res,200,null,"Found", dtArray, null,"detail");
                                                                                        }
                                                                                    }
                                                                                });
                                                                                connection.release();

                                                                            }
                                                                            else{
                                                                                data_response(req,res,200,null,"Not found",null,null,"update");
                                                                            }
                                                                        }
                                                                    });
                                                                    connection.release();
                                                                }
                                                            });
                                                                
                                                        }
                                                        else{   
                                                            data_response(req,res,200,null,"Not Found", dtArray, null,"detail");
                                                        }
                                                    }
                                                });
                                                connection.release();     
                                            }  
                                        }
                                        else{ 
                                            data_response(req,res,400,null,"Email / phone or password is wrong", rows, null,"detail");
                                        }
                                    }
                                });
                                connection.release();
                            }
                        }).catch(function (error) {
                            console.error(error);
                            data_response(req,res,401,err,error.response.data.message,null,null);
                        });
                    });
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/create', 
    upload_customer.fields([{
    name: 'image_idcard', maxCount: 1}, {name: 'image_selfie', maxCount: 1}, {name: 'image_profile', maxCount: 1}, {name: 'image_idcard_selfie', maxCount: 1} ]),
    function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/create');

    var params = {};

    params['fullname']=req.body.fullname ? req.body.fullname : null;
    params['email']=req.body.email ? req.body.email : null;
    params['phone']=req.body.phone ? req.body.phone : null;
    params['dob']=req.body.dob ? req.body.dob : null;
    params['gender']=req.body.gender ? req.body.gender : null;
    params['password']=req.body.password ? req.body.password : null;
    params['subject_id']=req.body.subject_id ? req.body.subject_id : null;
    params['recognition_enroll']=req.body.recognition_enroll ? req.body.recognition_enroll : null;
    params['application_id']=req.body.application_id ? req.body.application_id : null;
    params['status']=req.body.status ? req.body.status : null;
    params['flag_prod']=req.body.flag_prod ? req.body.flag_prod : null;

    params['ip']=ip.address();
    params['browser']=res.locals.ua.browser;
    params['os']=res.locals.ua.os;
    params['version']=res.locals.ua.version;
    params['description_activity']="customer create";
    params['branch_office_id']=req.body.branch_office_id ? req.body.branch_office_id : null;
    params['created_by']=req.body.created_by ? req.body.created_by : null;

    if(req.files){
        params['image_idcard']=req.files.image_idcard ? req.files.image_idcard[0].location : null;
        params['image_selfie']=req.files.image_selfie ? req.files.image_selfie[0].location : null;
        params['image_profile']=req.files.image_profile ? req.files.image_profile[0].location : null;
        params['image_idcard_selfie']=req.files.image_idcard_selfie ? req.files.image_idcard_selfie[0].location : null;
    }
    else{
        params['image_idcard']="";
        params['image_selfie']="";
        params['image_profile']="";
        params['image_idcard_selfie']="";
    }

    const token = req.headers['x-token'];

    var res_app_id="";
    var res_api_key="";
    var res_otp=null;
    if(params.flag_prod==1){
        res_app_id=config.vh_app_id_prod;
        res_api_key=config.vh_api_key_prod;
    }
    else{
        res_app_id=config.vh_app_id;
        res_api_key=config.vh_api_key;            
        res_otp="123123";
    }

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.fullname=="" || params.fullname==undefined || params.fullname==null){
                    data_response(req,res,400,null,'Fullname is required',null,null);
                }
                else if(params.email=="" || params.email==undefined || params.email==null){
                    data_response(req,res,400,null,'Email is required',null,null);
                }
                else if(params.phone=="" || params.phone==undefined || params.phone==null){
                    data_response(req,res,400,null,'Phone is required',null,null);
                }
                else if(params.dob=="" || params.dob==undefined || params.dob==null){
                    data_response(req,res,400,null,'Date of birth is required',null,null);
                }
                else if(params.gender=="" || params.gender==undefined || params.gender==null){
                    data_response(req,res,400,null,'Gender is required',null,null);
                }
                else if(params.application_id=="" || params.application_id==undefined || params.application_id==null){
                    data_response(req,res,400,null,'Application ID is required',null,null);
                }
                else if(params.status=="" || params.status==undefined || params.status==null){
                    data_response(req,res,400,null,'Status is required',null,null);
                }
                else{
                    database.getConnection(function(err,connection){
                        connection.config.namedPlaceholders=true;
                        params['password'] = bcrypt.hashSync(params.password, 10);

                        var query = "INSERT INTO customer (fullname, email, password, phone, dob, gender, image_idcard, image_selfie, image_profile, image_idcard_selfie, subject_id, recognition_enroll, application_id, status, created_date, created_by) VALUES (:fullname, :email, :password, :phone, :dob, :gender, :image_idcard, :image_selfie, :image_profile, :image_idcard_selfie, :subject_id, :recognition_enroll, :application_id, :status, NOW(), :created_by)";

                        connection.execute(query, params, function(err,rows,cols){
                            if(err){
                                if(err.sqlMessage.includes('email_exist')==true){
                                    data_response(req,res,400,err,'Email Exist',null,null);
                                }
                                else if(err.sqlMessage.includes('phone_exist')==true){
                                    data_response(req,res,400,err,'Phone exist',null,null);
                                }
                                else{
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{ 
                                if(rows["affectedRows"]>0){          
                                    
                                    var query = "SELECT id, fullname FROM customer WHERE phone=:phone LIMIT 1";
                        
                                    connection.execute(query, params, function(err, rows2, cols){
                                        if(err){
                                            data_response(req,res,500,err,null,null,connection.format(query,params));
                                        }
                                        else{
                                            if(rows2.length > 0){

                                                params['object_id']=rows2[0].id;
                                                params['object']=rows2[0].fullname;
                                                params['description']="";
                                                params['saldo']=0;
                                                params['user_flag']=1;

                                                var query = "INSERT INTO customer_history (customer_id, status, flag, created_by, created_date) VALUES (:object_id, :status, :user_flag, :created_by, NOW())";

                                                connection.execute(query, params, function(err,rows3,cols){
                                                    if(err){
                                                        data_response(req,res,500,err,null,connection.format(query,params));
                                                    }
                                                    else{ 
                                                        if(rows3["affectedRows"]>0){

                                                            var query = "INSERT INTO customer_maipay (fullname, email, phone, dob, gender, image_idcard, image_selfie, image_profile, image_idcard_selfie, subject_id, recognition_enroll, application_id, status, created_date, created_by, user_id, user_flag, saldo) VALUES (:fullname, :email, :phone, :dob, :gender, :image_idcard, :image_selfie, :image_profile, :image_idcard_selfie, :subject_id, :recognition_enroll, :application_id, :status, NOW(), :created_by, :object_id, :user_flag, :saldo)";
                                                        
                                                            connection.execute(query, params, function(err,rows5,cols){
                                                                if(err){
                                                                    if(err.sqlMessage.includes('email_exist')==true){
                                                                        data_response(req,res,400,err,'Email exist',null,null);
                                                                    }
                                                                    else if(err.sqlMessage.includes('phone_exist')==true){
                                                                        data_response(req,res,400,err,'Phone exist',null,null);
                                                                    }
                                                                    else{
                                                                        data_response(req,res,500,err,null,connection.format(query,params));
                                                                    }
                                                                }
                                                                else{ 
                                                                    if(rows5["affectedRows"]>0){
                                                                        
                                                                        var query = "INSERT INTO admin_activity (ip, browser, os, version, description, object, object_id, branch_office_id, created_date, created_by) VALUES (:ip, :browser, :os, :version, :description_activity, :object, :object_id, :branch_office_id, NOW(), :created_by)";
                                                            
                                                                        connection.execute(query, params, function(err,rows4,cols){
                                                                            if(err){
                                                                                data_response(req,res,500,err,null,connection.format(query,params));
                                                                            }
                                                                            else{ 
                                                                                if(rows4["affectedRows"]>0){

                                                                                    data_response(req,res,200,null,"Success",null,null,'create');

                                                                                    // const options = {
                                                                                    //     method: 'POST',
                                                                                    //     url: config.link_verihubs+'/v1/otp/send',
                                                                                    //     headers: {
                                                                                    //         Accept: 'application/json',
                                                                                    //         'App-ID': res_app_id,
                                                                                    //         'API-Key': res_api_key,
                                                                                    //         'Content-Type': 'application/json'
                                                                                    //     },
                                                                                    //     data: {
                                                                                    //         msisdn: params.phone, 
                                                                                    //         otp : res_otp,
                                                                                    //         time_limit: '60',
                                                                                    //     }
                                                                                    // };
                                                                        
                                                                                    // axios.request(options).then(function (response) {
                                                                                    //     params['response_otp'] = response.data.otp;

                                                                                    //     var query = "UPDATE customer SET otp_key=:response_otp WHERE phone=:phone";

                                                                                    //     connection.execute(query, params, function(err, rows5, cols){
                                                                                            
                                                                                    //         if(err){
                                                                                    //             data_response(req,res,500,err,null,connection.format(query,params));
                                                                                    //         }
                                                                                    //         else{
                                                                                    //             if(rows5["affectedRows"] > 0){

                                                                                    //                 response = {
                                                                                    //                     status : { code : 200, message : "Success"},
                                                                                    //                     data : { phone : params.phone }
                                                                                    //                 }    
                                                                                                    
                                                                                    //                 return utils.outputData(req, res, 200, err, response);
                                                                                    //             }
                                                                                    //             else{
                                                                                    //                 data_response(req,res,200,null,"Not found",null,null,"update");
                                                                                    //             }
                                                                                    //         }
                                                                                    //     });
                                                                                    //     connection.release();
                                                                                        
                                                                                    // }).catch(function (error) {
                                                                                    //     console.log(error);
                                                                                    //     data_response(req,res,401,err,error.response.data.message,null,null);
                                                                                    // });
                                                                                }
                                                                                else{
                                                                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                                                }
                                                                            }
                                                                        });
                                                                        connection.release();
                                                                    }
                                                                    else{
                                                                        data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                                    }
                                                                }
                                                            });
                                                            connection.release();
                                                        }
                                                        else{
                                                            data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                        }
                                                    }
                                                });
                                                connection.release();
                                            }
                                            else{   
                                                data_response(req,res,200,null,"Not found", rows2, null,"detail");
                                            }
                                        }
                                    });
                                    connection.release();
                                }
                                else{
                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                }
                            }
                        });
                        connection.release();

                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/read', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/read');
   
    var params = {};
    var res_query = "";
    var where = "";
    
    params["search"] = req.body.search ? req.body.search : null;
    params["status"] = req.body.status ? req.body.status.toString() : null;
    params["page"] = req.body.page ? req.body.page.toString() : "0";    
    params["offset"] = req.body.offset ? req.body.offset.toString() : "3000";
    params["flag_show"] = req.body.flag_show ? req.body.flag_show : null;
    params["date_start"] = req.body.date_start ? req.body.date_start : null;
    params["date_end"] = req.body.date_end ? req.body.date_end : null;
    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200) {		
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;

                    if(params.search){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' (customer.fullname LIKE CONCAT("%",:search,"%") OR customer.phone LIKE CONCAT("%",:search,"%") OR customer.email LIKE CONCAT("%",:search,"%")) ';
                    }

                    if(params.status){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' customer.status = :status ';
                    }

                    if(params.date_start && params.date_end){
                        console.log(1111);
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' DATE(customer.created_date) >= CONCAT(:date_start,"%") AND DATE(customer.created_date) <= CONCAT(:date_end,"%") ';
                    }
                    else if(params.date_start && params.date_end==null){
                        console.log(2222);
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' DATE(customer.created_date) >= CONCAT(:date_start,"%")';
                    }
                    else if(params.date_start==null && params.date_end){
                        console.log(3333);
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' DATE(customer.created_date) <= CONCAT(:date_end,"%") ';
                    }
                    
                    var res_show = "";
                    if(params.flag_show!=1){
                        res_show +=' ORDER BY customer.created_date DESC LIMIT :page,:offset ';
                    }
                    
                    var query_address = 'concat(\'"\', address.city_id, \'"\'), "$")';

                    var query = "SELECT (select GROUP_CONCAT((select name from branch_office where JSON_CONTAINS(branch_office.branch_id, "+query_address+")) from address where user_id=customer.id and user_flag=1 ) as address, customer.* FROM customer "+res_query+res_show;
                    console.log(connection.format(query,params));
                    connection.execute(query, params, function(err, rows, cols){
                        if(err){
                            data_response(req,res,500,err,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){
                                for (var i = 0; i < rows.length; i++) {
                                    if(rows[i].dob!=null || rows[i].dob!=undefined ){
                                        rows[i].dob=moment(rows[i].dob).format('DD MMM YYYY');
                                    }
                                    if(rows[i].created_date!=null || rows[i].created_date!=undefined ){
                                        rows[i].created_date=moment(rows[i].created_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    if(rows[i].updated_date!=null || rows[i].updated_date!=undefined ){
                                        rows[i].updated_date=moment(rows[i].updated_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                }
                                if(params.flag_show==1){
                                    data_response(req,res,200,null,"Found", rows, null, "total");
                                }
                                else{
                                    data_response(req,res,200,null,"Found", rows, null, "read");
                                }
                            }
                            else{   
                                data_response(req,res,200,null,"Not found", rows, null, "read");
                            }
                        }
                    });
                    connection.release();
                });
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/update', 
    upload_customer.fields([{
    name: 'image_idcard', maxCount: 1}, {name: 'image_selfie', maxCount: 1}, {name: 'image_profile', maxCount: 1}, {name: 'image_idcard_selfie', maxCount: 1} ]),
    function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/update');

    var params = {};

    params['id']=req.body.id ? req.body.id : null;
    params['fullname']=req.body.fullname ? req.body.fullname : null;
    params['email']=req.body.email ? req.body.email : null;
    params['phone']=req.body.phone ? req.body.phone : null;
    params['dob']=req.body.dob ? req.body.dob : null;
    params['gender']=req.body.gender ? req.body.gender : null;
    params['password']=req.body.password ? req.body.password : null;
    params['subject_id']=req.body.subject_id ? req.body.subject_id : null;
    params['recognition_enroll']=req.body.recognition_enroll ? req.body.recognition_enroll : null;
    params['application_id']=req.body.application_id ? req.body.application_id : null;
    params['status']=req.body.status ? req.body.status : null;

    params['ip']=ip.address();
    params['browser']=res.locals.ua.browser;
    params['os']=res.locals.ua.os;
    params['version']=res.locals.ua.version;
    params['description_activity']="customer update";
    params['branch_office_id']=req.body.branch_office_id ? req.body.branch_office_id : null;
    params['updated_by']=req.body.updated_by ? req.body.updated_by : null;

    if(req.files){
        params['image_selfie']=req.files.image_selfie ? req.files.image_selfie[0].location : req.body.image_selfie;
        params['image_idcard']=req.files.image_idcard ? req.files.image_idcard[0].location : req.body.image_idcard;
        params['image_profile']=req.files.image_profile ? req.files.image_profile[0].location : req.body.image_profile;
        params['image_idcard_selfie']=req.files.image_idcard_selfie ? req.files.image_idcard_selfie[0].location : req.body.image_idcard_selfie;
    }
    else{
        params['image_selfie']=req.body.image_selfie ? req.body.image_selfie : null;
        params['image_idcard']=req.body.image_idcard ? req.body.image_idcard : null;
        params['image_profile']=req.body.image_profile ? req.body.image_profile : null;
        params['image_idcard_selfie']=req.body.image_idcard_selfie ? req.body.image_idcard_selfie : null;
    }

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.fullname=="" || params.fullname==undefined || params.fullname==null){
                    data_response(req,res,400,null,'Fullname is required',null,null);
                }
                else if(params.email=="" || params.email==undefined || params.email==null){
                    data_response(req,res,400,null,'Email is required',null,null);
                }
                else if(params.phone=="" || params.phone==undefined || params.phone==null){
                    data_response(req,res,400,null,'Phone is required',null,null);
                }
                else if(params.dob=="" || params.dob==undefined || params.dob==null){
                    data_response(req,res,400,null,'Date of birth is required',null,null);
                }
                else if(params.gender=="" || params.gender==undefined || params.gender==null){
                    data_response(req,res,400,null,'Gender is required',null,null);
                }
                else if(params.application_id=="" || params.application_id==undefined || params.application_id==null){
                    data_response(req,res,400,null,'Application ID is required',null,null);
                }
                else if(params.status=="" || params.status==undefined || params.status==null){
                    data_response(req,res,400,null,'Status is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                
                        var res_pass='';
                        if(params.password!=null){
                            params["password"] = bcrypt.hashSync(params.password, 10); 
                            res_pass = 'password=:password,';
                        }
                
                        var query = "UPDATE customer SET fullname=:fullname, email=:email, "+res_pass+" phone=:phone, dob=:dob, gender=:gender, image_idcard=:image_idcard, image_selfie=:image_selfie, image_profile=:image_profile, image_idcard_selfie=:image_selfie, subject_id=:subject_id, application_id=:application_id, status=:status, updated_date=NOW(), updated_by=:updated_by WHERE id=:id";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                if(err.sqlMessage.includes('name_exist')==true){
                                    data_response(req,res,400,err,'Name exist',null,null);
                                }
                                else{
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                
                                    params['user_flag']=1;
                                    

                                    var query = "SELECT id, fullname FROM customer WHERE id=:id LIMIT 1";

                                    connection.execute(query, params, function(err, rows2, cols){
                                        if(err){
                                            data_response(req,res,500,err,null,null,connection.format(query,params));
                                        }
                                        else{
                                            if(rows2.length > 0){

                                                params['object_id']=rows2[0].id;
                                                params['object']=rows2[0].fullname;
                                                params['user_flag']=1;
                                                
                                                var query = "INSERT INTO admin_activity (ip, browser, os, version, description, object, object_id, branch_office_id, created_date, created_by) VALUES (:ip, :browser, :os, :version, :description_activity, :object, :object_id, :branch_office_id, NOW(), :updated_by)";
                
                                                console.log(connection.format(query,params))
                                    
                                                connection.execute(query, params, function(err,rows3,cols){
                                                    if(err){
                                                        data_response(req,res,500,err,null,connection.format(query,params));
                                                    }
                                                    else{ 
                                                        if(rows3["affectedRows"]>0){
                                                            data_response(req,res,200,null,"Success",null,null,'create');
                                                        }
                                                        else{
                                                            data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                                        }
                                                    }
                                                });
                                                connection.release();
                                            }
                                            else{   
                                                data_response(req,res,200,null,"Not found", rows2, null,"detail");
                                            }
                                        }
                                    });
                                    connection.release();
                
                                    // var query = "UPDATE customer_maipay SET fullname=:fullname, email=:email, "+res_pass+" phone=:phone, dob=:dob, gender=:gender, image_idcard=:image_idcard, image_selfie=:image_selfie, image_profile=:image_profile, image_idcard_selfie=:image_selfie, subject_id=:subject_id, application_id=:application_id, status=:status, updated_date=NOW(), updated_by=:updated_by, user_flag=:user_flag WHERE user_id=:id ";
                
                                    // connection.execute(query, params, function(err, rows2, cols){
                                    //     if(err){
                                    //         data_response(req,res,500,err,null,null,connection.format(query,params));
                                    //     }
                                    //     else{
                                    //         if(rows["affectedRows"] > 0){
                
                                    //             params['object_id']=rows2[0].id;
                                    //             params['object']=rows2[0].name;
                                                
                                    //             var query = "INSERT INTO admin_activity (ip, browser, os, version, description, object, object_id, branch_office_id, updated_date, updated_by) VALUES (:ip, :browser, :os, :version, :description_activity, :object, :object_id, :branch_office_id, NOW(), :updated_by)";
                
                                    //             console.log(connection.format(query,params))
                                    
                                    //             connection.execute(query, params, function(err,rows3,cols){
                                    //                 if(err){
                                    //                     data_response(req,res,500,err,null,connection.format(query,params));
                                    //                 }
                                    //                 else{ 
                                    //                     if(rows3["affectedRows"]>0){
                                    //                         data_response(req,res,200,null,"Success",null,null,'create');
                                    //                     }
                                    //                     else{
                                    //                         data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                    //                     }
                                    //                 }
                                    //             });
                                    //             connection.release();
                                    //         }
                                    //         else{   
                                    //             data_response(req,res,200,null,"Not found", rows2, null,"detail");
                                    //         }
                                    //     }
                                    // });
                                    // connection.release();
                                            
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"update");
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });
    }
});

route.post('/delete',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/delete');
    var params = {};

    params['id']=req.body.id ? req.body.id : null;

    params['ip']=ip.address();
    params['browser']=res.locals.ua.browser;
    params['os']=res.locals.ua.os;
    params['version']=res.locals.ua.version;
    params['description_activity']="news category delete";
    params['branch_office_id']=req.body.branch_office_id ? req.body.branch_office_id : null;
    params['deleted_by']=req.body.deleted_by ? req.body.deleted_by : null;


    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200) {		
                if(params.id=="" || params.id==undefined || params.id==null){
                    data_response(req,res,400,null,'ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;

                        var query = "SELECT id, name FROM customer WHERE id=:id LIMIT 1";

                        connection.execute(query, params, function(err, rows3, cols){
                            if(err){
                                data_response(req,res,500,err,null,null,connection.format(query,params));
                            }
                            else{
                                if(rows3.length > 0){

                                    params['object_id']=rows3[0].id;
                                    params['object']=rows3[0].name;
                                    
                                    var query = "INSERT INTO admin_activity (ip, browser, os, version, description, object, object_id, branch_office_id, created_date, created_by) VALUES (:ip, :browser, :os, :version, :description_activity, :object, :object_id, :branch_office_id, NOW(), :deleted_by)";
                        
                                    connection.execute(query, params, function(err,rows4,cols){
                                        if(err){
                                            data_response(req,res,500,err,null,connection.format(query,params));
                                        }
                                        else{ 
                                            if(rows4["affectedRows"]>0){

                                                var query = "DELETE FROM customer WHERE id=:id";

                                                connection.execute(query, params, function(err, rows, cols){
                                                    
                                                    if(err){
                                                        data_response(req,res,500,err,null,connection.format(query,params));
                                                    }
                                                    else{
                                                        if(rows["affectedRows"] > 0){
                                                            data_response(req,res,200,null,"Success",null,null,'delete');
                                                        }
                                                        else{
                                                            data_response(req,res,200,null,"Not found",null,null,"delete");
                                                        }
                                                    }
                                                });
                                                connection.release();
                                            }
                                            else{
                                                data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                            }
                                        }
                                    });
                                    connection.release();
                                }
                                else{   
                                    data_response(req,res,200,null,"Not found", rows3, null,"detail");
                                }
                            }
                        });
                        connection.release();
                                   
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

route.post('/view', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'customer/view');

    var params = {};

    params["id"] = req.body.id ? req.body.id : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200) {		
                if(params.id=="" || params.id==undefined || params.id==null){
                    data_response(req,res,400,null,'ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){       
                        connection.config.namedPlaceholders = true;
                        var query = "SELECT customer.*, customer_status.name as customer_status_name FROM customer JOIN customer_status ON customer_status.id=customer.status WHERE customer.id=:id LIMIT 1";
                        connection.execute(query, params, function(err, rows, cols){
                            if(err){
                                data_response(req,res,500,err,null,null,connection.format(query,params));
                            }
                            else{
                                if(rows.length > 0){
                                    if(rows[0].dob!=null || rows[0].dob!=undefined ){
                                        rows[0].dob=moment(rows[0].dob).format('DD MMM YYYY');
                                    }
                                    if(rows[0].created_date!=null || rows[0].created_date!=undefined ){
                                        rows[0].created_date=moment(rows[0].created_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    if(rows[0].updated_date!=null || rows[0].updated_date!=undefined ){
                                        rows[0].updated_date=moment(rows[0].updated_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    data_response(req,res,200,null,"Found", rows[0], null,"detail");
                                }
                                else{   
                                    data_response(req,res,200,null,"Not found", rows, null,"detail");
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/chart_customer_per_day_in_month', function (req, res) {

    console.log('\x1b[36m%s\x1b[0m', 'customer/chart_customer_per_day_in_month');

    var response = {};
    var params = {};
    const token = req.headers['x-token'];

    var res_query = "";
    params["date_start"] = req.body.date_start ? req.body.date_start : null;
    params["date_end"] = req.body.date_end ? req.body.date_end : null;

    var today = new Date();
    var yearnow = today.getFullYear();
    
    var monthnow = today.getMonth()+1;
    var res_monthnow='';

    if(monthnow<10){
        res_monthnow='0'+monthnow;
    }
    else{
        res_monthnow=monthnow;
    }

    var mon='';
    var showtgl='';
    var bulan=res_monthnow;
    var tahun=yearnow;
    var res_showtgl=[];
    var res_showtgl2=[];

    if(bulan=='01' || bulan=='03' || bulan=='05' || bulan=='07' || bulan=='08' || bulan=='10' || bulan=='12'){
        mon=31;
    }
    else if(bulan=="02"){
        if(parseInt(tahun) % 4 == 0){
            mon = 29
        }else{
            mon = 28
        }	
    }
    else{
        mon=30; 
    }
    for (var i = 1; i <= mon; i++) {
        
        if(i<10){
            showtgl='0'+i;
        }
        else{
            showtgl=i.toString();
        }
        res_showtgl.push(showtgl);

        res_showtgl2.push({
            tgl : showtgl,
            data : {
                created_date: null,
                count_date: 0
            }
        });
    }

    var last_res_showtgl=res_showtgl[res_showtgl.length-1];
    
    
    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		

                database.getConnection(function(err, connection) {							
                    connection.config.namedPlaceholders = true;
                    console.log(params.date_start)
                    if(params.date_start && params.date_end){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' SELECT created_date, COUNT(id) as count_date FROM customer WHERE (created_date BETWEEN  DATE_FORMAT(NOW() ,:date_start) AND DATE_FORMAT(NOW() ,:date_end)) '
                    }
                    
                    var query = "SELECT created_date, COUNT(id) as count_date FROM customer " +res_query+ "GROUP BY DATE(created_date)";

                    connection.execute(query, params, function(err, rows, cols) {
                        
                        if(err){
                            data_response(req,res,500,err,null,null,connection.format(query,params));
                        }
                        else{
                            rows.forEach(function(v,i){
                                if(rows[i]['created_date']){ rows[i]['created_date'] = moment(rows[i]['created_date']).format('DD'); }
                                res_showtgl2[parseInt(v.created_date)-1].data=v;
                            })
                            data_response(req,res,200,null,"Found", res_showtgl2, null, "read");
                        }
                    }); 
                    connection.release();
                    
                });
            } else {
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });
    }
});

function data_response(req,res,code,err,dtmessage,rows,dtquery,act){
    var response = {};
    if(code==200){
        if(act=="total"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length }
            }    
        }
        else if(act=="read"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length, rows }
            }    
        }
        else if(act=="detail"){
            response = {
                status : { code : code, message : dtmessage},
                data : rows 
            }    
        }
        else if(act=="delete" || act=="create" || act=="update"){
            response = {
                status : { code : code, message : dtmessage},
            }    
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==400){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==401){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else{
        response = {
            status : { code : 500, message : 'Internal Server Error', query:dtquery, err:err}
        }
        return utils.outputData(req, res, code, err, response);
    }
}

module.exports = route;