var utils = require('./utils');
var database = require('./database');
var express = require('express');
var route = express.Router();
var ip = require('ip');
var moment = require('moment');
var config = require('./config');
const axios = require('axios');
const bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
var ip = require('ip');

route.post('/read', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'category/read');
   
    var params = {};
    var res_query = "";
    var where = "";
    
    params["search"] = req.body.search ? req.body.search : null;
    params["status"] = req.body.status ? req.body.status.toString() : null;
    params["page"] = req.body.page ? req.body.page.toString() : "0";    
    params["offset"] = req.body.offset ? req.body.offset.toString() : "3000";
    params["flag_show"] = req.body.flag_show ? req.body.flag_show : null;
    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {			
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;

                    if(params.search){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' (category.fullname LIKE CONCAT("%",:search,"%") OR category.email LIKE CONCAT("%",:search,"%")) ';
                    }
                    
                    var res_show = "";
                    if(params.flag_show!=1){
                        res_show +=' ORDER BY category.created_date DESC LIMIT :page,:offset ';
                    }

                    var query = "SELECT category.* FROM category "+res_query+res_show;


                    console.log(connection.format(query,params));
                    connection.execute(query, params, function(err, rows, cols){
                        if(err){
                            data_response(req,res,500,err,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){
                                for (var i = 0; i < rows.length; i++) {
                                    if(rows[i].dob!=null || rows[i].dob!=undefined ){
                                        rows[i].dob=moment(rows[i].dob).format('DD MMM YYYY');
                                    }
                                    if(rows[i].created_date!=null || rows[i].created_date!=undefined ){
                                        rows[i].created_date=moment(rows[i].created_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    if(rows[i].updated_date!=null || rows[i].updated_date!=undefined ){
                                        rows[i].updated_date=moment(rows[i].updated_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                }
                                if(params.flag_show==1){
                                    data_response(req,res,200,null,"Found", rows, null, "total");
                                }
                                else{
                                    data_response(req,res,200,null,"Found", rows, null, "read");
                                }
                            }
                            else{   
                                data_response(req,res,200,null,"Not found", rows, null, "read");
                            }
                        }
                    });
                    connection.release();
                });
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/create',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'category/create');

    var params = {};
    
    params['name']=req.body.name ? req.body.name : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,401,null,'Name is required',null,null);
                }
                else{
                    database.getConnection(function(err,connection){
                        connection.config.namedPlaceholders=true;

                        var query = "INSERT INTO category (name, created_date) VALUES (:name, NOW())";
                        
                        console.log(connection.format(query,params));

                        connection.execute(query, params, function(err,rows,cols){
                            if(err){
                                if(err){
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{ 
                                if(rows["affectedRows"]>0){
                                    data_response(req,res,200,null,"Success",null,null,'create');
                                }
                                else{
                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/update', function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'category/update');

    var params = {};

    params['id']=req.body.id ? req.body.id : null;
    params['name']=req.body.name ? req.body.name : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,400,null,'Name is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                
                        var query = "UPDATE category SET name=:name, updated_date=NOW() WHERE id=:id";

                        console.log(connection.format(query,params))
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                if(err){
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                                    data_response(req,res,200,null,"Success",null,null,'update');         
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"update");
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });
    }
});

route.post('/delete',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'category/delete');
    var params = {};
    params['id']=req.body.id ? req.body.id : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.id=="" || params.id==undefined || params.id==null){
                    data_response(req,res,401,null,'ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                        
                        var query = "DELETE FROM category WHERE id=:id";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                data_response(req,res,500,err,null,connection.format(query,params));
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                                    data_response(req,res,200,null,"Success",null,null,"delete");
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"delete");
                                }
                            }
                        });
                        connection.release();
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

function data_response(req,res,code,err,dtmessage,rows,dtquery,act){
    var response = {};
    if(code==200){
        if(act=="total"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length }
            }    
        }
        else if(act=="read"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length, rows }
            }    
        }
        else if(act=="detail"){
            response = {
                status : { code : code, message : dtmessage},
                data : rows 
            }    
        }
        else if(act=="delete" || act=="create" || act=="update"){
            response = {
                status : { code : code, message : dtmessage},
            }    
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==400){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==401){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else{
        response = {
            status : { code : 500, message : 'Internal Server Error', query:dtquery, err:err}
        }
        return utils.outputData(req, res, code, err, response);
    }
}

module.exports = route;