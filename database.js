'use strict';

var config = require('./config');
var mysql = require('mysql2');

var pool = mysql.createPool({
    connectionLimit : 10,
    host : config.db_host,
    user : config.db_user,
    password : config.db_pass,
    database : config.db_name,
    port : config.db_port, 
    debug : false,
    multipleStatements : true
});

module.exports = pool