var utils = require('./utils');
var database = require('./database');
var express = require('express');
var route = express.Router();
var ip = require('ip');
var moment = require('moment');
var config = require('./config');
const axios = require('axios');
const bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
var ip = require('ip');

route.post('/login', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'users/login');
    var dtArray = [];
    var params = {};

    params["email"] = req.body.email ? req.body.email : null;
    params["password"] = req.body.password ? req.body.password : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;

                    var query = "SELECT users.* from users WHERE email=:email LIMIT 1";
                    console.log(connection.format(query,params))
                    
                    connection.execute(query, params, function(err, rows, cols){
                        
                        if(err){
                            data_response(req,res,500,err,null,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){

                                params["ress_password"] = rows[0].password;
                                
                                var res_token="";
                                    
                                if(params.token_short==1){
                                    res_token=config.token_expired;
                                }
                                else{
                                    res_token=config.token_expired_mobile;
                                }
                                
                                var token = jwt.sign(
                                    {
                                        id: rows[0]['id']
                                    }, 
                                    config.secret_key, { 
                                        expiresIn: res_token
                                    }
                                );

                                bcrypt.compare(params.password, params.ress_password, function (err, result) {
                                    if (result == true) {
                                        dtArray = { 
                                            id:rows[0].id,
                                            user_name:rows[0].name,
                                            user_email:rows[0].email,
                                            created_date:rows[0].created_date,
                                            user_point:rows[0].point,
                                            token:token,
                                            expired:res_token,
                                        };
                                        data_response(req,res,200,null,"Found", dtArray, null,"detail");
                                    } else {
                                        data_response(req,res,400,null,"Email / phone or password is wrong", rows, null,"detail");
                                    }
                                });

                            }
                            else{ 
                                data_response(req,res,400,null,"Email / phone or password is wrong", rows, null,"detail");
                            }
                        }
                    });
                    connection.release();
                });
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/read', function(req, res){
    console.log('\x1b[36m%s\x1b[0m', 'users/read');
   
    var params = {};
    var res_query = "";
    var where = "";
    
    params["search"] = req.body.search ? req.body.search : null;
    params["status"] = req.body.status ? req.body.status.toString() : null;
    params["page"] = req.body.page ? req.body.page.toString() : "0";    
    params["offset"] = req.body.offset ? req.body.offset.toString() : "3000";
    params["flag_show"] = req.body.flag_show ? req.body.flag_show : null;
    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {			
                database.getConnection(function(err, connection){
                    connection.config.namedPlaceholders = true;

                    if(params.search){
                        if (where != "") {
                            where = " AND ";
                        }
                        else {
                            where = " WHERE ";
                        }
                        res_query += where +' (users.fullname LIKE CONCAT("%",:search,"%") OR users.email LIKE CONCAT("%",:search,"%")) ';
                    }
                    
                    var res_show = "";
                    if(params.flag_show!=1){
                        res_show +=' ORDER BY users.created_date DESC LIMIT :page,:offset ';
                    }

                    var query = "SELECT users.* FROM users "+res_query+res_show;


                    console.log(connection.format(query,params));
                    connection.execute(query, params, function(err, rows, cols){
                        if(err){
                            data_response(req,res,500,err,null,connection.format(query,params));
                        }
                        else{
                            if(rows.length > 0){
                                for (var i = 0; i < rows.length; i++) {
                                    if(rows[i].dob!=null || rows[i].dob!=undefined ){
                                        rows[i].dob=moment(rows[i].dob).format('DD MMM YYYY');
                                    }
                                    if(rows[i].created_date!=null || rows[i].created_date!=undefined ){
                                        rows[i].created_date=moment(rows[i].created_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                    if(rows[i].updated_date!=null || rows[i].updated_date!=undefined ){
                                        rows[i].updated_date=moment(rows[i].updated_date).format('DD MMM YYYY, HH:mm:ss');
                                    }
                                }
                                if(params.flag_show==1){
                                    data_response(req,res,200,null,"Found", rows, null, "total");
                                }
                                else{
                                    data_response(req,res,200,null,"Found", rows, null, "read");
                                }
                            }
                            else{   
                                data_response(req,res,200,null,"Not found", rows, null, "read");
                            }
                        }
                    });
                    connection.release();
                });
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });        
    }
});

route.post('/create',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'users/create');

    var params = {};
    
    params['name']=req.body.name ? req.body.name : null;
    params['email']=req.body.email ? req.body.email : null;
    params['password']=req.body.password ? req.body.password : null;
    params['point']=req.body.point ? req.body.point : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,401,null,'Name is required',null,null);
                }
                else if(params.email=="" || params.email==undefined || params.email==null){
                    data_response(req,res,401,null,'Email is required',null,null);
                }
                else if(params.password=="" || params.password==undefined || params.password==null){
                    data_response(req,res,401,null,'Password is required',null,null);
                }
                else{
                    database.getConnection(function(err,connection){
                        connection.config.namedPlaceholders=true;
                        
                        params["password"] = bcrypt.hashSync(params.password, 10); 

                        var query = "INSERT INTO users (name, password, email, point, created_date) VALUES (:name, :password, :email, :point, NOW())";
                        
                        console.log(connection.format(query,params));

                        connection.execute(query, params, function(err,rows,cols){
                            if(err){
                                if(err.sqlMessage.includes('email_exist')==true){
                                    data_response(req,res,401,err,'Email exist',null,null);
                                }
                                else{
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{ 
                                if(rows["affectedRows"]>0){

                                    data_response(req,res,200,null,"Success",null,null,'create');
                                }
                                else{
                                    data_response(req,res,500,err,"Fail",null,connection.format(query,params));
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        }); 
    }
});

route.post('/update', function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'users/update');

    var params = {};

    params['id']=req.body.id ? req.body.id : null;
    params['name']=req.body.name ? req.body.name : null;
    params['email']=req.body.email ? req.body.email : null;
    params['password']=req.body.password ? req.body.password : null;
    params['point']=req.body.point ? req.body.point : null;

    params['ip']=ip.address();
    params['browser']=res.locals.ua.browser;
    params['os']=res.locals.ua.os;
    params['version']=res.locals.ua.version;
    params['updated_by']=req.body.updated_by ? req.body.updated_by : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.name=="" || params.name==undefined || params.name==null){
                    data_response(req,res,400,null,'Name is required',null,null);
                }
                else if(params.email=="" || params.email==undefined || params.email==null){
                    data_response(req,res,400,null,'Email is required',null,null);
                }
                else if(params.password=="" || params.password==undefined || params.password==null){
                    data_response(req,res,400,null,'Password is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                
                        var res_pass='';
                        if(params.password!=null){
                            params["password"] = bcrypt.hashSync(params.password, 10); 
                            res_pass = 'password=:password,';
                        }
                
                        var query = "UPDATE users SET name=:name, email=:email, "+res_pass+" updated_date=NOW(), ip=:ip, browser=:browser, os=:os, version=:version, point=:point WHERE id=:id";

                        console.log(connection.format(query,params))
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                if(err.sqlMessage.includes('name_exist')==true){
                                    data_response(req,res,400,err,'Name exist',null,null);
                                }
                                else{
                                    data_response(req,res,500,err,null,connection.format(query,params));
                                }
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                                    data_response(req,res,200,null,"Success",null,null,'update');         
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"update");
                                }
                            }
                        });
                        connection.release();
                    });
                }
            }   
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });
    }
});

route.post('/delete',function(req,res){
    console.log('\x1b[36m%s\x1b[0m', 'users/delete');
    var params = {};
    params['id']=req.body.id ? req.body.id : null;

    const token = req.headers['x-token'];

    if(token=="" || token==undefined){
        data_response(req,res,401,null,'Header token undefined',null,null);
    }
    else{
        utils.checkToken (token, function(result) {
            if (result.statusCode == 200 || token==config.token_code) {		
                if(params.id=="" || params.id==undefined || params.id==null){
                    data_response(req,res,401,null,'ID is required',null,null);
                }
                else{
                    database.getConnection(function(err, connection){
                        connection.config.namedPlaceholders=true;
                        
                        var query = "DELETE FROM users WHERE id=:id";
                        
                        connection.execute(query, params, function(err, rows, cols){
                            
                            if(err){
                                data_response(req,res,500,err,null,connection.format(query,params));
                            }
                            else{
                                if(rows["affectedRows"] > 0){
                        
                                    data_response(req,res,200,null,"Success",null,null,"delete");
                                }
                                else{
                                    data_response(req,res,200,null,"Not found",null,null,"delete");
                                }
                            }
                        });
                        connection.release();
                    })
                }
            }
            else{
                data_response(req,res,result.statusCode,null,result.message,null,null);
            }
        });    
    }
});

function data_response(req,res,code,err,dtmessage,rows,dtquery,act){
    var response = {};
    if(code==200){
        if(act=="total"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length }
            }    
        }
        else if(act=="read"){
            response = {
                status : { code : code, message : dtmessage},
                data : { total:rows.length, rows }
            }    
        }
        else if(act=="detail"){
            response = {
                status : { code : code, message : dtmessage},
                data : rows 
            }    
        }
        else if(act=="delete" || act=="create" || act=="update"){
            response = {
                status : { code : code, message : dtmessage},
            }    
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==400){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else if(code==401){
        response = {
            status : { code : code, message : dtmessage}
        }
        return utils.outputData(req, res, code, err, response);
    }
    else{
        response = {
            status : { code : 500, message : 'Internal Server Error', query:dtquery, err:err}
        }
        return utils.outputData(req, res, code, err, response);
    }
}

module.exports = route;