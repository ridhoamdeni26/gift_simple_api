<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Simple API GIFT</h3>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

I made a simple API, Simple API GIFT is made for some who want to use or create a simple API with nodeJS and ExpressJS. in it there are several login APIs and also CRUD from users and CRUD from gifts themselves, users who will make redeem points and give ratings if after successfully redeeming the product

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

this is the framework i am using or some NPM i am using for this simple gift API:


* [Node.js](https://nodejs.dev/)
* [Express.js](https://expressjs.com/)

some NPM I use:

* [Express.js - Body Parser](http://expressjs.com/en/resources/middleware/body-parser.html)
* [Cors](https://www.npmjs.com/package/cors)
* [Json Web Token](https://www.npmjs.com/package/jsonwebtoken)
* [Mysql2](https://www.npmjs.com/package/mysql2)
* [Crypto.Js](https://www.npmjs.com/package/crypto-js)
* [Express Useragent](https://www.npmjs.com/package/express-useragent)
* [IP](https://www.npmjs.com/package/ip)
* [Moment](https://www.npmjs.com/package/moment)
* [Node Mailer](https://www.npmjs.com/package/nodemailer)
* [Axios](https://www.npmjs.com/package/axios)
* [Nodemon](https://www.npmjs.com/package/nodemon)

use database:

* [Mysql](https://www.mysql.com/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
make sure you have installed node on the local you have.

* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Get a free API Key at [https://example.com](https://example.com)
2. Clone the repo
   ```sh
   git clone https://ridhoamdeni26@bitbucket.org/ridhoamdeni26/gift_simple_api.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. If there is a problem run this command
   ```sh
   npm install --save express body-parser cors jsonwebtoken mysql2 crypto-js express-useragent ip moment nodemailer axios nodemon
   ```
5. import rolling glory.sql here I use XAMPP
   ```sh
   Import Database rollingglomer.sql
   ```
6. Enter your API in `config.js` check the database matches the name
   ```js
   config.db_name = process.env.DB_NAME
   ```
7. Run using nodemon
   ```sh
   nodemon app.js
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

I use nodeJs and Express JS because it is very suitable for applications that require real time communication between client and server. Node.js uses the concept of a non-blocking I/O model. This concept makes processing in Node.js more efficient because it is not locked while a process is running.

I use the Mysql database because Mysql can run stably on various operating systems Windows, Linux, Sun Os, Mac, Unix, and many more, Can handle large-scale databases with more than 50 million records, with queries that are quite easy for me to understand.

The optimization database that I use only runs every table optimize that is done in Phpmyadmin

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Ridho Amdeni - [@ridhoamdeni](https://www.instagram.com/ridhoamdeni/) - email@example.com

Project Link: [https://ridhoamdeni26@bitbucket.org/ridhoamdeni26/gift_simple_api.git](https://ridhoamdeni26@bitbucket.org/ridhoamdeni26/gift_simple_api.git)

<p align="right">(<a href="#top">back to top</a>)</p>
