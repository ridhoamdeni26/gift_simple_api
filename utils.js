'use strict';

var config = require('./config');
const jwt = require('jsonwebtoken');

module.exports.outputData = function(req, res, status_code, err, data){
    var token = req.headers['x-token'];
    res.status(status_code);
    checkToken(token, function(result) {
		console.log(config.app_name +' | ' + status_code +' | ' +  config.app_host + ":"+ config.app_port +  req.baseUrl + req.url + ' | ' + JSON.stringify(req.body) + ' | ' + JSON.stringify(data));
		res.status(status_code).json(data).send();
	});
};

function checkToken (token, result) {
	if (token == undefined) {
		result({
			statusCode: 401,
			message: "Header x-token must provided"
		});
	} else {
		jwt.verify(token, config.secret_key, function(err, decoded) {
			if (err) {
				if (err.name == 'TokenExpiredError') {
					result({
						statusCode: 401,
						message: 'Token expired'
					});
				} else if (err.name == 'JsonWebTokenError') {
					result({
						statusCode: 400,
						message: err.message
					});
				}
			} else {
				result({
					statusCode: 200,
					userId: decoded.id_admin,
					expires_in: decoded.exp - Math.floor(Date.now() / 1000),
					message: 'OK'
				});
			}
		});
	}
}

module.exports.checkToken = (token, result) => {
    if (token == undefined) {
		result({
			statusCode: 401,
			message: "Header x-token must provided"
		});
	} else {
		jwt.verify(token, config.secret_key, function(err, decoded) {
			if (err) {
				if (err.name == 'TokenExpiredError') {
					result({
						statusCode: 401,
						message: 'Token expired'
					});
				} else if (err.name == 'JsonWebTokenError') {
					result({
						statusCode: 400,
						message: err.message
					});
				}
			} else {
				result({
					statusCode: 200,
					userId: decoded.id_admin,
					expires_in: decoded.exp - Math.floor(Date.now() / 1000),
					message: 'OK'
				});
			}
		});
	}
}